import { Injectable } from '@angular/core';
import { Usuario } from '../model/usuario.model';
import { SERVICE } from './app.api';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CanActivate, Router } from '@angular/router';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService implements CanActivate{
    public usuario: Usuario;

    private classe = 'usuario';

    filtroNomeUsuario = '';
    filtroCPF = '';
    filtroStatus = '';

    constructor(
      protected http: HttpClient,
      private storage: StorageService) {
      if (!this.usuario) {
        this.usuario    = new Usuario();
        this.usuario.id = 0;
      }
    }

    canActivate(){
      return this.storage.get('userJson').then(userJson => {
        if (userJson) {this.usuario = JSON.parse(userJson) as Usuario;}

        return true;
      });
    }

    existeEmail(usuario: Usuario){
      return this.http.post(`${SERVICE}/${this.classe}/existeEmail`, JSON.stringify(usuario)) as Observable<number>;
    }

    existeLogin(usuario: Usuario){
      return this.http.post(`${SERVICE}/${this.classe}/existeLogin`, JSON.stringify(usuario)) as Observable<boolean>;
    }

    verificaCodigoUsuario(codigo: string){
      return this.http.post(`${SERVICE}/${this.classe}/verificaCodigoUsuario`, JSON.stringify(codigo)) as Observable<Usuario>;
  }

    existeCpf(usuario: Usuario){
      return this.http.post(`${SERVICE}/${this.classe}/existeCpf`, JSON.stringify(usuario)) as Observable<number>;
    }

    login(usuario: Usuario) {
      return this.http.post(`${SERVICE}/${this.classe}/login`, JSON.stringify(usuario)) as Observable<Usuario>;
    }

  usuarioLogado(): boolean {
    if (this.usuario !== undefined && this.usuario.id !== null && this.usuario.id > 0) {
      return false;
    } else {
        return true;
    }
  }

    resetarSenha(usuario: Usuario){
      return this.http.post(`${SERVICE}/${this.classe}/resetarSenha`, usuario.id) as Observable<Usuario[]>;
    }

    ativarUsuario(usuario: Usuario){
      return this.http.post(`${SERVICE}/${this.classe}/ativarUser`, usuario.id) as Observable<Usuario>;
    }

    desativarUsuario(usuario: Usuario){
      return this.http.post(`${SERVICE}/${this.classe}/desativarUser`, usuario.id) as Observable<Usuario>;
    }

    ativarTodosUsuarios(usuarios: Usuario[]){
      return this.http.post(`${SERVICE}/${this.classe}/ativarTodos`, JSON.stringify(usuarios)) as Observable<Usuario[]>;
    }

    desativarTodosUsuarios(usuarios: Usuario[]){
      return this.http.post(`${SERVICE}/${this.classe}/desativarTodos`, JSON.stringify(usuarios)) as Observable<Usuario[]>;
    }

    getAll(){
      return this.http.post(`${SERVICE}/${this.classe}/getAll`, this.usuario.id) as Observable<Usuario[]>;
    }

    getTodosUsuarios(){
      return this.http.post(`${SERVICE}/${this.classe}/getTodosUsuarios`, this.usuario.id) as Observable<Usuario[]>;
    }

    getUsuario(){
      let id = 0;

      if(this.usuario.id){
        id = this.usuario.id;
      }

      return (this.http.post(`${SERVICE}/${this.classe}/getUsuarioById`, id) as Observable<Usuario>)
        .toPromise()
        .then(data => {
            this.usuario = data;
            this.storage.set('userJson', JSON.stringify(data));
            
            return this.usuario;
        });
    }

    getUsuarioById(usuario: Usuario){
      return this.http.post(`${SERVICE}/${this.classe}/getUsuarioById`, usuario.id) as Observable<Usuario>;
    }

    atualizarUsuario(id: number) {
      return this.http.post(`${SERVICE}/${this.classe}/getUsuarioById`, id) as Observable<Usuario>;
    }

    salvar(usuario: Usuario) {
      return this.http.post(`${SERVICE}/${this.classe}/salvar`, JSON.stringify(usuario)) as Observable<any>;
    }
}
