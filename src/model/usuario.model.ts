

export class Usuario {
  id: number;
  nome: string;
  senha: string;
  email: string;
  telefone: string;
  ultimoAcesso: Date;
  dataNascimento: string;
  cpf: string;
  nomeMae: string;
  status: boolean;
  desativarUsuario: boolean;
  senhaAtual: string;
  dataInclusao: Date;
  dataAlteracao: Date;
}
