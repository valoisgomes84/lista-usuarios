import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.page.html',
  styleUrls: ['./usuario.page.scss'],
})
export class UsuarioPage implements OnInit {

  constructor(
    public NavCtlr : NavController
  ) { }

  ngOnInit() {
  }

  openConsultarUser() {
    this.NavCtlr.navigateRoot ('/consultar-usuario' , {});
  }

  openAlterarUser() {
    this.NavCtlr.navigateRoot ('/alterar-usuario' , {});
  }

  openAlterarSenha() {
    this.NavCtlr.navigateRoot ('/alterar-senha' , {});
  }

}
