import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  public home: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    public NavCtlr : NavController
  ) { }

  ngOnInit() {
    this.home = this.activatedRoute.snapshot.paramMap.get('id');
  }

  openRegister() {
    this.NavCtlr.navigateRoot ('/cadastrar-usuario' , {});
  }
  openLogin() {
    this.NavCtlr.navigateRoot ('/login' , {});
  }

}
