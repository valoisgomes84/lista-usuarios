import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsultarUsuarioPage } from './consultar-usuario.page';

const routes: Routes = [
  {
    path: '',
    component: ConsultarUsuarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsultarUsuarioPageRoutingModule {}
