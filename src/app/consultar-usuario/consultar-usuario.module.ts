import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsultarUsuarioPageRoutingModule } from './consultar-usuario-routing.module';

import { ConsultarUsuarioPage } from './consultar-usuario.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsultarUsuarioPageRoutingModule
  ],
  declarations: [ConsultarUsuarioPage]
})
export class ConsultarUsuarioPageModule {}
