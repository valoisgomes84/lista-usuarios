import { Usuario } from 'src/model/usuario.model';
import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-consultar-usuario',
  templateUrl: './consultar-usuario.page.html',
  styleUrls: ['./consultar-usuario.page.scss'],
})
export class ConsultarUsuarioPage implements OnInit {
  usuarioPag: Usuario[] = [];
  user: Usuario[] = [];
  tamanhoInfiniteScroll = 4;

  constructor(
    public NavCtlr : NavController,
    public loadCtrl: LoadingController,
    ) { }

  ngOnInit() {
  }

  openHome() {
    this.NavCtlr.navigateRoot ('home/inbox' , {});
  }

  openAddPage() {
    this.NavCtlr.navigateRoot ('/cadastrar-usuario' , {});
  }

  verUser() {
    this.NavCtlr.navigateRoot ('/usuario' , {});
  }

  doInfinite(infiniteScroll){

    setTimeout(() => {
      if(this.usuarioPag.length === this.user.length){
        return infiniteScroll.target.complete();
      }

      this.usuarioPag.push(...this.user.slice(this.usuarioPag.length, this.usuarioPag.length + this.tamanhoInfiniteScroll));
      infiniteScroll.target.complete();
    }, 500);
  }

}
