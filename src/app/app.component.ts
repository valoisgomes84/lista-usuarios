import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Consultar Usuario', url: '/consultar-usuario', icon: 'search-circle' },
  ];
  constructor() {}
}
