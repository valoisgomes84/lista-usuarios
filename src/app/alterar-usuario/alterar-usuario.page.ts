import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-alterar-usuario',
  templateUrl: './alterar-usuario.page.html',
  styleUrls: ['./alterar-usuario.page.scss'],
})
export class AlterarUsuarioPage implements OnInit {

  constructor(
    public NavCtlr : NavController
  ) { }

  ngOnInit() {
  }

  openUser() {
    this.NavCtlr.navigateRoot ('/usuario' , {});
  }

}
