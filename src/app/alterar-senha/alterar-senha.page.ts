import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-alterar-senha',
  templateUrl: './alterar-senha.page.html',
  styleUrls: ['./alterar-senha.page.scss'],
})
export class AlterarSenhaPage implements OnInit {

  constructor(
    public NavCtlr : NavController
  ) { }

  ngOnInit() {
  }

  openUser() {
    this.NavCtlr.navigateRoot ('/usuario' , {});
  }

}
