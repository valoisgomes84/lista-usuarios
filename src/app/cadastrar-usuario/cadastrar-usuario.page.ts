import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-cadastrar-usuario',
  templateUrl: './cadastrar-usuario.page.html',
  styleUrls: ['./cadastrar-usuario.page.scss'],
})
export class CadastrarUsuarioPage implements OnInit {

  constructor(public NavCtlr : NavController) { }

  ngOnInit() {
  }

  openHome() {
    this.NavCtlr.navigateRoot ('home/inbox' , {});
  }

  openConsultarUser() {
    this.NavCtlr.navigateRoot ('/consultar-usuario' , {});
  }

}
