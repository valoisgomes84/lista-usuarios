import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'lista-usuarios',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
