import { AppPage } from './app.po';

describe('new App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });
  describe('default screen', () => {
    beforeEach(() => {
      page.navigateTo('/inbox');
    });
    it('should say inbox', () => {
      expect(page.getParagraphText()).toContain('inbox');
    });
  });
});
